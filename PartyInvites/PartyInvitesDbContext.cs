﻿using PartyInvites.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PartyInvites
{
    public class PartyInvitesDbContext : DbContext
    {
        public DbSet<GuestResponse> GuestResponse { get; set; }
    }
}