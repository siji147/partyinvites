﻿using PartyInvites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartyInvites
{
    public class GuestResponseRepository : IDisposable
    {
        private PartyInvitesDbContext partyInvitesDbContext;

        public GuestResponseRepository()
        {
            partyInvitesDbContext = new PartyInvitesDbContext();
        }

        public IEnumerable<GuestResponse> GetAllGuestResponses()
        {
            return partyInvitesDbContext.GuestResponse.ToList();
        }

        public GuestResponse GetGuestResponseWithId(int id)
        {
            return partyInvitesDbContext.GuestResponse.FirstOrDefault(x => x.Id == id);
        }

        public void UpdateGuestResponse(GuestResponse guestResponse)
        {
            GuestResponse newGuestResponse = partyInvitesDbContext.GuestResponse.FirstOrDefault(x => x.Id == guestResponse.Id);
            newGuestResponse.Name = guestResponse.Name;
            newGuestResponse.Email = guestResponse.Email;
            newGuestResponse.Phone = guestResponse.Phone;
            newGuestResponse.WillAttend = guestResponse.WillAttend;

            partyInvitesDbContext.SaveChanges();
        }

        public void InsertNewGuestResponse(GuestResponse guestResponse)
        {
            partyInvitesDbContext.GuestResponse.Add(guestResponse);
            partyInvitesDbContext.SaveChanges();
        }

        public void DeleteGuestResponse(int id)
        {
            GuestResponse guestResponseToBeDeleted = partyInvitesDbContext.GuestResponse.FirstOrDefault(x => x.Id == id);
            partyInvitesDbContext.GuestResponse.Remove(guestResponseToBeDeleted);
            partyInvitesDbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    partyInvitesDbContext.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}