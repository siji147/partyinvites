﻿using PartyInvites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace PartyInvites.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            int hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Good morning" : "Good afternoon";
            return View();
        }

        [HttpGet]
        public ActionResult RsvpForm()
        {
            return View();
        }
        private List<GuestResponse> allGuests;
        [HttpPost]
        public ActionResult RsvpForm([Bind(Exclude = "Id")]GuestResponse guestResponse)
        {

            //TODO: Email response to party organizer
            if (ModelState.IsValid)
            {
                try
                {
                    using (GuestResponseRepository guestResponseRepository = new GuestResponseRepository())
                {
                    guestResponseRepository.InsertNewGuestResponse(guestResponse);
                    allGuests = guestResponseRepository.GetAllGuestResponses().ToList();
                }
                
                    string messageWillAttend = $"Thanks for attending, {guestResponse.Name}. <br /> You will eat and drink a lot. Lol";
                    string messageWillNotAttend = $"Thanks for filling the form, {guestResponse.Name} but Why won't you attend my party na?";
                    StringBuilder messageBody = new StringBuilder();
                    messageBody.Append($"<html><body>" +
                            $"<table>" +
                            $"<tr><th>Name</th><th>Email</th><th>Phone</th><th>WillAttend</th></tr>");

                    foreach (var guestresponse in allGuests)
                    {



                        messageBody.Append($"<tr><td>{guestresponse.Name}</td><td>{guestresponse.Email}</td><td>{guestresponse.Phone}</td><td>{guestresponse.WillAttend}</td></tr>");
                           
                        
                    }
                    messageBody.Append($"</table></body></html>");

                    WebMail.SmtpServer = "smtp.gmail.com";
                    WebMail.SmtpPort = 587;
                    WebMail.EnableSsl = true;
                    WebMail.UserName = "sijiajayi1@gmail.com";
                    WebMail.Password = "Abioye1992";
                    WebMail.From = "partyinvites@noreply.com";
                    WebMail.Send(guestResponse.Email, "Thanks for RSVPing, " + guestResponse.Name,
                        (bool)guestResponse.WillAttend ? messageWillAttend : messageWillNotAttend, "noreply@noreply.com", "sijiajayi1@gmail.com");

                    WebMail.Send("sijiajayi1@gmail.com", "Someone just Filled your form", messageBody.ToString(),

                        isBodyHtml: true);
                    //(bool)guestResponse.WillAttend ? messageWillAttend : messageWillNotAttend, "noreply@noreply.com", "sijiajayi1@gmail.com");
                    //Model.Name + " is " + ((Model.WillAttend ?? false) ? "" : "not")
                    //+ "attending");
                    //}
                    

                }

                catch (Exception)
                {
                    Response.Write("Sorry we couldn't send the email to confirm your RSVP.");
                }
                return View("Thanks", guestResponse);
            }
            else
            {
                //there is a validation error
                return View();
            }
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}