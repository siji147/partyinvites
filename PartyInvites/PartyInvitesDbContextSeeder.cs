﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PartyInvites.Models;

namespace PartyInvites
{
    public class PartyInvitesDbContextSeeder : DropCreateDatabaseIfModelChanges<PartyInvitesDbContext>
    {
        protected override void Seed(PartyInvitesDbContext context)
        {
            GuestResponse guestResponse1 = new GuestResponse()
            {
                Email = "sijiajayi1@gmail.com",
                Name = "Sijibomi",
                Phone = "07082263122",
                WillAttend = true
            };


            GuestResponse guestResponse2 = new GuestResponse()
            {
                Email = "sijiajayi1@yahoo.com",
                Name = "Abioye",
                Phone = "07039810313",
                WillAttend = false
            };

            context.GuestResponse.Add(guestResponse1);
            context.GuestResponse.Add(guestResponse2);

            base.Seed(context);
        }
    }
}